﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Rpg_Character_Generator;
using System.IO;
using System.Data.SqlClient;

namespace RpgChar1
{
    public partial class Form1 : Form
    {
        public string charType = "abc";
        public Form1()
        {
            InitializeComponent();
        }

        private void btnTheif_CheckedChanged(object sender, EventArgs e)
        {
            if (nameValue == ""){
                MessageBox.Show("You must enter a name!");
                btnTheif.Checked = false;
            }
            else {
            summaryHider();
            btnSub1.Text = "Archer";
            btnSub2.Text = "Assassin";
            hiddenLabel.Visible = true;
            btnSub1.Visible = true;
            btnSub2.Visible = true;
            
                }
        }

        private void btnMage_CheckedChanged(object sender, EventArgs e)
        {
            if (nameValue == ""){
                MessageBox.Show("You must enter a name!");
                btnMage.Checked = false;
            }
            else {
            summaryHider();
            btnSub1.Text = "Necromancer";
            btnSub2.Text = "Spellsword";
            hiddenLabel.Visible = true;
            btnSub1.Visible = true;
            btnSub2.Visible = true;
            
        }
            }

        private void btnWarrior_CheckedChanged(object sender, EventArgs e)
        {
            if (nameValue == ""){
                MessageBox.Show("You must enter a name!");
                btnWarrior.Checked = false;
            }
            else {
            summaryHider();
            btnSub1.Text = "Barbarian";
            btnSub2.Text = "Paladin";
            hiddenLabel.Visible = true;
            btnSub1.Visible = true;
            btnSub2.Visible = true;
            
        }
            }
        public string nameValue = "";

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            nameValue = textBox1.Text;
        }

        private void btnSub2_CheckedChanged(object sender, EventArgs e)
        {
            
            if (btnSub2.Text == "Paladin")
            {
                summaryHider();
                charType = btnSub2.Text;
                Paladin paladin1 = new Paladin(nameValue);
                charNameLabel.Text = paladin1.Name;
                charHealthLabel.Text = "" + paladin1.Health;
                charManaLabel.Text = "" + paladin1.Mana;
                charArmorLabel.Text = "" + paladin1.Armor;
                charAtckdmgLabel.Text = "" + paladin1.AttackDamage;

            }
            if (btnSub2.Text == "Assassin")
            {
                summaryHider();
                charType = btnSub2.Text;
                Assassin assassin1 = new Assassin(nameValue);
                charNameLabel.Text = assassin1.Name;
                charHealthLabel.Text = "" + assassin1.Health;
                charManaLabel.Text = "" + assassin1.Mana;
                charArmorLabel.Text = "" + assassin1.Armor;
                charAtckdmgLabel.Text = "" + assassin1.AttackDamage;

            }
            if (btnSub2.Text == "Spellsword")
            {
                summaryHider();
                charType = btnSub2.Text;
                Spellsword spellsword1 = new Spellsword(nameValue);
                charNameLabel.Text = spellsword1.Name;
                charHealthLabel.Text = "" + spellsword1.Health;
                charManaLabel.Text = "" + spellsword1.Mana;
                charArmorLabel.Text = "" + spellsword1.Armor;
                charAtckdmgLabel.Text = "" + spellsword1.AttackDamage;

            }
            

        }

        private void btnSub1_CheckedChanged(object sender, EventArgs e)
        {
            if (btnSub1.Text == "Barbarian")
            {
                summaryHider();
                charType = btnSub1.Text;
                Barbarian barbarian1 = new Barbarian(nameValue);
                charNameLabel.Text = barbarian1.Name;
                charHealthLabel.Text = "" + barbarian1.Health;
                charManaLabel.Text = "" + barbarian1.Mana;
                charArmorLabel.Text = "" + barbarian1.Armor;
                charAtckdmgLabel.Text = "" + barbarian1.AttackDamage;
            }

            if (btnSub1.Text == "Necromancer")
            {
                summaryHider();
                charType = btnSub1.Text;
                Necromancer necromancer1 = new Necromancer(nameValue);
                charNameLabel.Text = necromancer1.Name;
                charHealthLabel.Text = "" + necromancer1.Health;
                charManaLabel.Text = "" + necromancer1.Mana;
                charArmorLabel.Text = "" + necromancer1.Armor;
                charAtckdmgLabel.Text = "" + necromancer1.AttackDamage;
            }

            if (btnSub1.Text == "Archer")
            {
                summaryHider();
                charType = btnSub1.Text;
                Archer archer1 = new Archer(nameValue);
                charNameLabel.Text = archer1.Name;
                charHealthLabel.Text = "" + archer1.Health;
                charManaLabel.Text = "" + archer1.Mana;
                charArmorLabel.Text = "" + archer1.Armor;
                charAtckdmgLabel.Text = "" + archer1.AttackDamage;
            }
        }
       

        private void btnCreate_Click(object sender, EventArgs e)
        {
            
            
           
            if (!btnSub1.Checked && !btnSub2.Checked)
            {
                MessageBox.Show("You must select a subclass!");
            }
            
            else
            {
                button1.Visible = true;
                charNameLabel.Visible = true;
                charHealthLabel.Visible = true;
                charManaLabel.Visible = true;
                charArmorLabel.Visible = true;
                charAtckdmgLabel.Visible = true;
                label2.Visible = true;
                label4.Visible = true;
                label6.Visible = true;
                label8.Visible = true;
                label10.Visible = true;
            }
        }

      /*  private void button1_Click(object sender, EventArgs e)
        {
            string summary =
                (label2.Text + " " + charNameLabel.Text + "\n" +
                label4.Text + " " + charHealthLabel.Text + "\n" +
                label6.Text + " "  + charArmorLabel.Text + "\n" +
                label8.Text + " " + charManaLabel.Text + "\n" +
                label10.Text + " " + charAtckdmgLabel.Text
                );

            string docPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            File.WriteAllText(docPath + "\\character.txt", summary);
        } */


        private void button1_Click(object sender, EventArgs e) {

            string connetionString = null;
            SqlConnection cnn;
            connetionString = @"Data Source=PC7397\SQLEXPRESS;Initial Catalog=Characters; Trusted_Connection=True";
            cnn = new SqlConnection(connetionString);
            cnn.Open();

            string query = "INSERT INTO CharTable3 (Name, Type, Health, Mana, Armor, AttackDamage)";
            query += " VALUES (@Name, @Type, @Health, @Mana, @Armor, @AttackDamage)";

            SqlCommand myCommand = new SqlCommand(query, cnn);
            myCommand.Parameters.AddWithValue("@Name", charNameLabel.Text);
            myCommand.Parameters.AddWithValue("@Type", charType);
            myCommand.Parameters.AddWithValue("@Health", charHealthLabel.Text);
            myCommand.Parameters.AddWithValue("@Mana", charManaLabel.Text);
            myCommand.Parameters.AddWithValue("@Armor", charArmorLabel.Text);
            myCommand.Parameters.AddWithValue("@AttackDamage", charAtckdmgLabel.Text);

            myCommand.ExecuteNonQuery();

            summaryHider();
        }



        public void summaryHider(){
            button1.Visible = false;
                charNameLabel.Visible = false;
                charHealthLabel.Visible = false;
                charManaLabel.Visible = false;
                charArmorLabel.Visible = false;
                charAtckdmgLabel.Visible = false;
                label2.Visible = false;
                label4.Visible = false;
                label6.Visible = false;
                label8.Visible = false;
                label10.Visible = false;
        
        }

      
    }
}
