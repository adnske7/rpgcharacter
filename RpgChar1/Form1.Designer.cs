﻿namespace RpgChar1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.label1 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.btnWarrior = new System.Windows.Forms.RadioButton();
            this.btnMage = new System.Windows.Forms.RadioButton();
            this.btnTheif = new System.Windows.Forms.RadioButton();
            this.btnSub2 = new System.Windows.Forms.RadioButton();
            this.btnSub1 = new System.Windows.Forms.RadioButton();
            this.label2 = new System.Windows.Forms.Label();
            this.charNameLabel = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.charHealthLabel = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.charManaLabel = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.charArmorLabel = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.charAtckdmgLabel = new System.Windows.Forms.Label();
            this.btnCreate = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.hiddenLabel = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(37, 61);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Name :";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(102, 55);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(100, 26);
            this.textBox1.TabIndex = 1;
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // btnWarrior
            // 
            this.btnWarrior.AutoSize = true;
            this.btnWarrior.Location = new System.Drawing.Point(27, 158);
            this.btnWarrior.Name = "btnWarrior";
            this.btnWarrior.Size = new System.Drawing.Size(85, 24);
            this.btnWarrior.TabIndex = 2;
            this.btnWarrior.TabStop = true;
            this.btnWarrior.Text = "Warrior";
            this.btnWarrior.UseVisualStyleBackColor = true;
            this.btnWarrior.CheckedChanged += new System.EventHandler(this.btnWarrior_CheckedChanged);
            // 
            // btnMage
            // 
            this.btnMage.AutoSize = true;
            this.btnMage.Location = new System.Drawing.Point(118, 158);
            this.btnMage.Name = "btnMage";
            this.btnMage.Size = new System.Drawing.Size(74, 24);
            this.btnMage.TabIndex = 3;
            this.btnMage.TabStop = true;
            this.btnMage.Text = "Mage";
            this.btnMage.UseVisualStyleBackColor = true;
            this.btnMage.CheckedChanged += new System.EventHandler(this.btnMage_CheckedChanged);
            // 
            // btnTheif
            // 
            this.btnTheif.AutoSize = true;
            this.btnTheif.Location = new System.Drawing.Point(209, 158);
            this.btnTheif.Name = "btnTheif";
            this.btnTheif.Size = new System.Drawing.Size(69, 24);
            this.btnTheif.TabIndex = 4;
            this.btnTheif.TabStop = true;
            this.btnTheif.Text = "Thief";
            this.btnTheif.UseVisualStyleBackColor = true;
            this.btnTheif.CheckedChanged += new System.EventHandler(this.btnTheif_CheckedChanged);
            // 
            // btnSub2
            // 
            this.btnSub2.AutoSize = true;
            this.btnSub2.Location = new System.Drawing.Point(205, 278);
            this.btnSub2.Name = "btnSub2";
            this.btnSub2.Size = new System.Drawing.Size(69, 24);
            this.btnSub2.TabIndex = 5;
            this.btnSub2.TabStop = true;
            this.btnSub2.Text = "sub2";
            this.btnSub2.UseVisualStyleBackColor = true;
            this.btnSub2.Visible = false;
            this.btnSub2.CheckedChanged += new System.EventHandler(this.btnSub2_CheckedChanged);
            // 
            // btnSub1
            // 
            this.btnSub1.AutoSize = true;
            this.btnSub1.Location = new System.Drawing.Point(41, 278);
            this.btnSub1.Name = "btnSub1";
            this.btnSub1.Size = new System.Drawing.Size(69, 24);
            this.btnSub1.TabIndex = 6;
            this.btnSub1.TabStop = true;
            this.btnSub1.Text = "sub1";
            this.btnSub1.UseVisualStyleBackColor = true;
            this.btnSub1.Visible = false;
            this.btnSub1.CheckedChanged += new System.EventHandler(this.btnSub1_CheckedChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(537, 237);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 20);
            this.label2.TabIndex = 7;
            this.label2.Text = "Name: ";
            this.label2.Visible = false;
            // 
            // charNameLabel
            // 
            this.charNameLabel.AutoSize = true;
            this.charNameLabel.Location = new System.Drawing.Point(615, 237);
            this.charNameLabel.Name = "charNameLabel";
            this.charNameLabel.Size = new System.Drawing.Size(51, 20);
            this.charNameLabel.TabIndex = 8;
            this.charNameLabel.Text = "label3";
            this.charNameLabel.Visible = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(537, 266);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(60, 20);
            this.label4.TabIndex = 9;
            this.label4.Text = "Health:";
            this.label4.Visible = false;
            // 
            // charHealthLabel
            // 
            this.charHealthLabel.AutoSize = true;
            this.charHealthLabel.Location = new System.Drawing.Point(615, 266);
            this.charHealthLabel.Name = "charHealthLabel";
            this.charHealthLabel.Size = new System.Drawing.Size(51, 20);
            this.charHealthLabel.TabIndex = 10;
            this.charHealthLabel.Text = "label5";
            this.charHealthLabel.Visible = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(537, 301);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(53, 20);
            this.label6.TabIndex = 11;
            this.label6.Text = "Mana:";
            this.label6.Visible = false;
            // 
            // charManaLabel
            // 
            this.charManaLabel.AutoSize = true;
            this.charManaLabel.Location = new System.Drawing.Point(615, 301);
            this.charManaLabel.Name = "charManaLabel";
            this.charManaLabel.Size = new System.Drawing.Size(51, 20);
            this.charManaLabel.TabIndex = 12;
            this.charManaLabel.Text = "label7";
            this.charManaLabel.Visible = false;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(537, 337);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(56, 20);
            this.label8.TabIndex = 13;
            this.label8.Text = "Armor:";
            this.label8.Visible = false;
            // 
            // charArmorLabel
            // 
            this.charArmorLabel.AutoSize = true;
            this.charArmorLabel.Location = new System.Drawing.Point(615, 337);
            this.charArmorLabel.Name = "charArmorLabel";
            this.charArmorLabel.Size = new System.Drawing.Size(51, 20);
            this.charArmorLabel.TabIndex = 14;
            this.charArmorLabel.Text = "label9";
            this.charArmorLabel.Visible = false;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(537, 373);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(76, 20);
            this.label10.TabIndex = 15;
            this.label10.Text = "Atckdmg:";
            this.label10.Visible = false;
            // 
            // charAtckdmgLabel
            // 
            this.charAtckdmgLabel.AutoSize = true;
            this.charAtckdmgLabel.Location = new System.Drawing.Point(615, 373);
            this.charAtckdmgLabel.Name = "charAtckdmgLabel";
            this.charAtckdmgLabel.Size = new System.Drawing.Size(60, 20);
            this.charAtckdmgLabel.TabIndex = 16;
            this.charAtckdmgLabel.Text = "label11";
            this.charAtckdmgLabel.Visible = false;
            // 
            // btnCreate
            // 
            this.btnCreate.Location = new System.Drawing.Point(118, 349);
            this.btnCreate.Name = "btnCreate";
            this.btnCreate.Size = new System.Drawing.Size(112, 44);
            this.btnCreate.TabIndex = 17;
            this.btnCreate.Text = "Create";
            this.btnCreate.UseVisualStyleBackColor = true;
            this.btnCreate.Click += new System.EventHandler(this.btnCreate_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(37, 23);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(237, 20);
            this.label3.TabIndex = 18;
            this.label3.Text = "Insert the name of our character";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(64, 135);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(190, 20);
            this.label5.TabIndex = 19;
            this.label5.Text = "Chose your charactertype";
            // 
            // hiddenLabel
            // 
            this.hiddenLabel.AutoSize = true;
            this.hiddenLabel.Location = new System.Drawing.Point(23, 239);
            this.hiddenLabel.Name = "hiddenLabel";
            this.hiddenLabel.Size = new System.Drawing.Size(276, 20);
            this.hiddenLabel.TabIndex = 20;
            this.hiddenLabel.Text = "Choose your characters specialization";
            this.hiddenLabel.Visible = false;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(541, 406);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(125, 32);
            this.button1.TabIndex = 21;
            this.button1.Text = "Save";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Visible = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.hiddenLabel);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btnCreate);
            this.Controls.Add(this.charAtckdmgLabel);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.charArmorLabel);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.charManaLabel);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.charHealthLabel);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.charNameLabel);
            this.Controls.Add(this.btnSub1);
            this.Controls.Add(this.btnSub2);
            this.Controls.Add(this.btnTheif);
            this.Controls.Add(this.btnMage);
            this.Controls.Add(this.btnWarrior);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.RadioButton btnWarrior;
        private System.Windows.Forms.RadioButton btnMage;
        private System.Windows.Forms.RadioButton btnTheif;
        private System.Windows.Forms.RadioButton btnSub2;
        private System.Windows.Forms.RadioButton btnSub1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label charNameLabel;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label charHealthLabel;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label charManaLabel;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label charArmorLabel;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label charAtckdmgLabel;
        private System.Windows.Forms.Button btnCreate;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label hiddenLabel;
        private System.Windows.Forms.Button button1;
    }
}

