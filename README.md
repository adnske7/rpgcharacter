# Rpg Character Generator
A small program that allows the user to create a character of a given class and subclass. The user will be able to name the character, display the stats and add the text to a .txt file on the users computer.

## Getting Started
Open the project in Visual studio, and run the Program.cs file. (If you use the "save to .txt" function the file will be created in "Documents" on your computer)

## Author
* **Ådne Skeie** - [adnske7](https://gitlab.com/adnske7)
